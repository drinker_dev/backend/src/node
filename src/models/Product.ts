import { Document, model, Model, Schema, Types } from "mongoose";

import { IBrand } from "./Brand";
import { ICountry } from "./Country";

export const ProductSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    brand: {
        type: Types.ObjectId,
        ref: "Brand",
        required: true
    },
    presentation: {
        type: [String]
    },
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export interface IProduct extends Document {
    country: ICountry;
    brand: IBrand;
    name: string;
    presentation: string[];
    createdAt: Date;
    updatedAt: Date;
}

export const Product: Model<IProduct> = model<IProduct>("Product", ProductSchema, "products");

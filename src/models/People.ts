import { Document, model, Model, Schema, Types } from "mongoose";

import { ICity } from "./City";
import { ICountry } from "./Country";
import { IPoint, SchemaPoint } from "./GeoJSON";

export const PeopleSchema = new Schema ({
    user: {
        type: Types.ObjectId,
        ref: "User",
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    identityDocument: {
        type: String,
        required: true
    },
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: Types.ObjectId,
        ref: "City",
        required: true
    },
    address: {
        type: String,
        required: true
    },
    geoLocation: SchemaPoint,
    status: {
        type: Number,
        enum: [0, 1, 2],
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export interface IPeople extends Document {
    user: string;
    identityDocument: string;
    firstName: string;
    lastName: string;
    age: number;
    country: ICountry;
    city: ICity;
    address: string;
    geoLocation: IPoint;
    status: number;
    createdAt: Date;
    updatedAt: Date;
}

export const People: Model<IPeople> = model<IPeople>("People", PeopleSchema, "people");

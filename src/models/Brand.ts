import { Document, model, Model, Schema } from "mongoose";

export const BrandSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    enabled: {
        type: Boolean
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export interface IBrand extends Document {
    name: string;
    enabled: boolean;
    createdAt: Date;
    updatedAt: Date;
}

export const Brand: Model<IBrand> = model<IBrand>("Brand", BrandSchema, "brands");

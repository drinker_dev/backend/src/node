import { Document, model, Model, Schema, Types } from "mongoose";

import { IBrand } from "./Brand";
import { IProduct } from "./Product";

export const ProductDetailSchema = new Schema ({
    _id: {
        type: Types.ObjectId,
        ref: "Product",
        required: true
    },
    brand: {
        type: Types.ObjectId,
        ref: "Brand",
        required: true
    },
    presentation: String,
    price: {
        type: Number,
        required: true
    }
},
{
    timestamps: {
        updatedAt: "updatedAt"
    },
    _id: false
});

export interface IProductDetail extends Document {
    product: IProduct;
    brand: IBrand;
    presentation: string;
    updatedAt: Date;
}

export const ProductDetail: Model<IProductDetail> = model<IProductDetail>("ProductDetail", ProductDetailSchema);

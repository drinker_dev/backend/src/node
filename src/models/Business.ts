import { Document, model, Model, Schema, Types } from "mongoose";

import { ICity } from "./City";
import { ICountry } from "./Country";
import { IPoint, SchemaPoint } from "./GeoJSON";

export const BusinessSchema = new Schema ({
    user: {
        type: Types.ObjectId,
        ref: "User",
    },
    name: {
        type: String,
        required: true
    },
    rif: {
        type: String,
        required: true,
        unique: true
    },
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: Types.ObjectId,
        ref: "City",
        required: true
    },
    address: {
        type: String,
        required: true
    },
    geoLocation: SchemaPoint,
    status: {
        type: Number,
        enum: [0, 1, 2],
        default: 0
    }
},
{
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});

export interface IBusiness extends Document {
    user: string;
    name: string;
    rif: string;
    country: ICountry;
    city: ICity;
    address: string;
    geoLocation: IPoint;
    status: number;
    createdAt: Date;
    updatedAt: Date;
}

export const Business: Model<IBusiness> = model<IBusiness>("Business", BusinessSchema, "business");

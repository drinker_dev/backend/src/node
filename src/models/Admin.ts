import { Document, model, Model, Schema, Types } from "mongoose";

import { ICity } from "./City";
import { ICountry } from "./Country";

export const AdminSchema = new Schema ({
    user: {
        type: Types.ObjectId,
        ref: "User",
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    identityDocument: {
        type: String,
        required: true
    },
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: Types.ObjectId,
        ref: "City",
        required: true
    },
    address: {
        type: String,
        required: true
    },
    status: {
        type: Number,
        enum: [0, 1, 2],
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export interface IAdmin extends Document {
    user: string;
    firstName: string;
    lastName: string;
    identityDocument: string;
    age: number;
    country: ICountry;
    city: ICity;
    status: number;
    createdAt?: Date;
    updatedAt?: Date;
}

export const Admin: Model<IAdmin> = model<IAdmin>("Admin", AdminSchema, "administrations");

import { Document, model, Model, Schema, Types } from "mongoose";

import { IBrand } from "./Brand";
import { IBusiness } from "./Business";
import { ICity } from "./City";
import { ICountry } from "./Country";
import { FeedBusiness } from "./FeedBusiness";
import { FeedProduct } from "./FeedProduct";
import { IProduct } from "./Product";

export const FeedHomeSchema = new Schema ({
    business: {
        type: Types.ObjectId,
        ref: "Business",
        required: true
    },
    brand: {
        type: Types.ObjectId,
        ref: "Brand",
        required: true
    },
    product: {
        type: Types.ObjectId,
        ref: "Product",
        required: true
    },
    presentation: String,
    price: {
        type: Number,
        required: true
    },
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: Types.ObjectId,
        ref: "City",
        required: true
    }
},
{
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});

export interface IFeedHome extends Document {
    business: IBusiness;
    brand: IBrand;
    product: IProduct;
    presentation: string;
    price: number;
    country: ICountry;
    city: ICity;
    createdAt: Date;
    updatedAt: Date;
}

FeedHomeSchema.post("save", async (doc) => {
    let product;
    let business;
    if (! await FeedProduct.exists({ "product": doc.get("product"), "business._id": doc.get("business") })) {
        product = {
            brand: doc.get("brand"),
            product: doc.get("product"),
            presentation: doc.get("presentation"),
            country: doc.get("country"),
            city: doc.get("city"),
            createdAt: doc.get("createdAt"),
            updatedAt: doc.get("updatedAt")
        };
        business = {
            business: {
                _id: doc.get("business"),
                price: doc.get("price"),
                updatedAt: doc.get("updatedAt")
            }
        };
        await FeedProduct.updateOne(
            {
                product: doc.get("product")
            },
            {
                $set: product,
                $addToSet: business
            },
            {
                upsert: true
            }
        );
    }

    if (! await FeedBusiness.exists({ "business": doc.get("business"), "products._id": doc.get("product") })) {
        business = {
            business: doc.get("business"),
            country: doc.get("country"),
            city: doc.get("city"),
            createdAt: doc.get("createdAt"),
            updatedAt: doc.get("updatedAt")
        };
        product = {
            products: {
                _id: doc.get("product"),
                brand: doc.get("brand"),
                presentation: doc.get("presentation"),
                price: doc.get("price"),
                updatedAt: doc.get("updatedAt")
            }
        };
        await FeedBusiness.updateOne(
            {
                business: doc.get("business")
            },
            {
                $set: business,
                $addToSet: product
            },
            {
                upsert: true
            }
        );
    }
});

FeedHomeSchema.post("updateOne", async function() {
    const docToUpdate = await this.model.findOne(this.getQuery());
    await FeedProduct.updateOne(
        {
            "product": docToUpdate.product,
            "business._id": docToUpdate.business
        },
        {
            $set: {
                "business.$.price": docToUpdate.price,
                "business.$.updatedAt": docToUpdate.updatedAt
            }
        }
    );

    await FeedBusiness.updateOne(
        {
            "business": docToUpdate.business,
            "products._id": docToUpdate.product
        },
        {
            $set: {
                "products.$.price": docToUpdate.price,
                "products.$.updatedAt": docToUpdate.updatedAt
            }
        }
    );
});

FeedHomeSchema.post("remove", async (doc) => {
    await FeedProduct.updateOne(
        {
            product: doc.get("product")
        },
        {
            $pull: {
                business: {
                    _id: doc.get("business")
                }
            }
        }
    );
    await FeedBusiness.updateOne(
        {
            business: doc.get("business")
        },
        {
            $pull: {
                products: {
                    _id: doc.get("product")
                }
            }
        }
    );
});

export const FeedHome: Model<IFeedHome> = model<IFeedHome>("FeedHome", FeedHomeSchema, "feed_home");

import { Document, Schema } from "mongoose";

export interface IPoint extends Document {
  type: string;
  coordinates: number[];
}

export const SchemaPoint = new Schema({
  type: {
    $type: String,
    enum: ["Point"],
    required: true
  },
  coordinates: {
    $type: [Number],
    required: true
  }
},
{
  _id: false,
  typeKey: "$type",
});

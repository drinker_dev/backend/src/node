import { Document, model, Model, Schema, Types } from "mongoose";

const UserSchema = new Schema ({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: String,
    enabled: {
        type: Boolean,
        default: false
    },
    type: {
        type: String,
        required: true,
        enum: ["Admin", "Business", "People"]
    },
    role: [String],
    profile: {
        type: Types.ObjectId,
        required: true,
        // Instead of a hardcoded model name in `ref`, `refPath` means Mongoose
        // will look at the `onModel` property to find the right model.
        refPath: "type"
    }
},
{
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});

export interface IUser extends Document {
    _id: string;
    username: string;
    password: string;
    enabled: boolean;
    role: string[];
    type: string;
    profile: object;
}

export const User: Model<IUser> = model<IUser>("User", UserSchema, "users");

declare global {
    namespace Express {
        // tslint:disable-next-line: no-empty-interface
        interface User extends Document {}
    }
}

import { Document, model, Model, Schema, Types } from "mongoose";

import { ICity } from "./City";
import { IPoint, SchemaPoint } from "./GeoJSON";

export const CountrySchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    iso: String,
    geoLocation: SchemaPoint,
    cities: {
        type: Types.ObjectId,
        ref: "City",
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export interface ICountry extends Document {
    id: string;
    name: string;
    iso?: string;
    geoLocation?: IPoint;
    cities: ICity[];
    createdAt?: Date;
    updatedAt?: Date;
}

export const Country: Model<ICountry> = model<ICountry>("Country", CountrySchema, "countries");

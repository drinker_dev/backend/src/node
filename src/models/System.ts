import { model, Schema } from "mongoose";

const SystemSchema = new Schema ({
    menu: {
        type: Array
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export const System = model("System", SystemSchema, "system");

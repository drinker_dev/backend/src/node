import { Document, model, Model, Schema, Types } from "mongoose";
import { IBusiness } from "./Business";

export const BusinessDetailSchema = new Schema ({
    _id: {
        type: Types.ObjectId,
        ref: "Business",
        required: true
    },
    price: {
        type: Number,
        required: true
    }
},
{
    timestamps: {
        updatedAt: "updatedAt"
    },
    _id: false
});

export interface IBusinessDetail extends Document {
    business: IBusiness;
    price: number;
    updatedAt: Date;
}

export const BusinessDetail: Model<IBusinessDetail> = model<IBusinessDetail>("BusinessDetail", BusinessDetailSchema);

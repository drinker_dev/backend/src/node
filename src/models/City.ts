import { Document, model, Model, Schema, Types } from "mongoose";

import { ICountry } from "./Country";
import { IPoint, SchemaPoint } from "./GeoJSON";

export const CitySchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    state: String,
    capital: String,
    geoLocation: SchemaPoint,
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

export interface ICity extends Document {
    id: string;
    name: string;
    country: ICountry;
    state?: string;
    capital?: string;
    geoLocation?: IPoint;
    createdAt?: Date;
    updatedAt?: Date;
}

export const City: Model<ICity> = model<ICity>("City", CitySchema, "cities");

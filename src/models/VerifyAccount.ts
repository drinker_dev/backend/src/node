import { Document, model, Model, Schema, Types } from "mongoose";

const VerifyAccountSchema = new Schema ({
    user: {
        type: Types.ObjectId,
        ref: "User",
        required: true,
        unique: true
    },
    token: {
        type: String,
        required: true
    }
},
{
    timestamps: {
        createdAt: "createdAt"
    }
});

export interface IVerifyAccount extends Document {
    _id: string;
    user: string;
    token: string;
    createdAt: Date;
}

export const VerifyAccount: Model<IVerifyAccount> = model<IVerifyAccount>("VerifyAccount", VerifyAccountSchema, "verify_accounts");

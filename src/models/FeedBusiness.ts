import { Document, model, Model, Schema, Types } from "mongoose";

import { IBusiness } from "./Business";
import { ICity } from "./City";
import { ICountry } from "./Country";
import { IProductDetail, ProductDetailSchema } from "./ProductDetail";

export const FeedBusinessSchema = new Schema ({
    business: {
        type: Types.ObjectId,
        ref: "Business",
        required: true
    },
    average: {
        type: Number
    },
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: Types.ObjectId,
        ref: "City",
        required: true
    },
    products: [ProductDetailSchema]
},
{
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});

export interface IFeedBusiness extends Document {
    business: IBusiness;
    products: IProductDetail;
    average: number;
    country: ICountry;
    city: ICity;
    createdAt: Date;
    updatedAt: Date;
}

export const FeedBusiness: Model<IFeedBusiness> = model<IFeedBusiness>("FeedBusiness", FeedBusinessSchema, "feed_business");

import { Document, model, Model, Schema, Types } from "mongoose";

import { IBrand } from "./Brand";
import { BusinessDetailSchema, IBusinessDetail } from "./BusinessDetail";
import { ICity } from "./City";
import { ICountry } from "./Country";
import { IProduct } from "./Product";

export const FeedProductSchema = new Schema ({
    brand: {
        type: Types.ObjectId,
        ref: "Brand",
        required: true
    },
    product: {
        type: Types.ObjectId,
        ref: "Product",
        required: true
    },
    presentation: String,
    average: {
        type: Number
    },
    business: [BusinessDetailSchema],
    country: {
        type: Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: Types.ObjectId,
        ref: "City",
        required: true
    }
},
{
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});

export interface IFeedProduct extends Document {
    product: IProduct;
    brand: IBrand;
    presentation: string;
    business: IBusinessDetail[];
    country: ICountry;
    city: ICity;
    createdAt: Date;
    updatedAt: Date;
}

export const FeedProduct: Model<IFeedProduct> = model<IFeedProduct>("FeedProduct", FeedProductSchema, "feed_products");

import mailgun from "mailgun-js";

export class Mailable  {
    public from: string;
    public to: string | string[];
    private readonly conexion: mailgun.Mailgun;

    constructor(from: string, to: string | string[]) {
        this.conexion = mailgun({
            apiKey: process.env.MAIL_API_KEY,
            domain: process.env.MAIL_DOMAIN
        });
        this.from = from;
        this.to = to;
    }

    /**
     * send email
     * @param subject subject of mail
     * @param message body of mail
     * @param type indicate if the message is text or html
     */
    public async send(subject: string, message: string, type: string = "html") {
        try {
            await this.conexion.messages().send({
                from: this.from,
                to: this.convertTargetToString(this.to),
                subject,
                [type]: message
            });
        } catch (e) {
            console.error(`error sending emial, ${e}`);
        }
    }

    /**
     * convert the value from array type to string type, if It is an array
     * @param value this can be string or array type
     * @return string
     */
    private convertTargetToString(value: string | string[]): string {
        if (typeof value === "object") {
            return value.toString();
        }
        return value;
    }
}

import { Router } from "express";

type Wrapper = ((router: Router ) => void);

export class Middleware {
    constructor(private middleware: Wrapper[], private router: Router) {}

    public apply() {
        for (const f of this.middleware) {
          f(this.router);
        }
    }
}

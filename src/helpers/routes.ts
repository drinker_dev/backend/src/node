import { NextFunction, Request, Response, Router } from "express";

type Handler = (req: Request, res: Response, next: NextFunction) => Promise<void> | void;

interface IRoute {
    path: string;
    method: string;
    handler: Handler | Handler[];
}

export class Routes {
    constructor(private routes: IRoute[], private router: Router) {}

    public apply() {
        for (const route of this.routes) {
            const { method, path, handler } = route;
            (this.router as any)[method](`/api/v1/${path}`, handler);
        }
    }
}

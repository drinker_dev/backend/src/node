import mongoose from "mongoose";
import { database } from "../keys";

mongoose.set("useCreateIndex", true);

export class Database {
    public async connect() {
        try {
            await mongoose.connect(database.URI, { useNewUrlParser: true, useUnifiedTopology: true });
            console.log(`>>> DB is connected`);
        } catch (err) {
            console.error(err);
        }
    }
}

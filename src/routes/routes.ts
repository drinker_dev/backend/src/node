import { Request, Response } from "express";

import { BrandController } from "../controllers/admin/BrandController";
import { BusinessController } from "../controllers/admin/BusinessController";
import { ProductController } from "../controllers/admin/ProductController";
import { AuthController } from "../controllers/auth/authController";
import { VerifyAccountController } from "../controllers/auth/VerifyAccountController";
import { ProductController as BusinessProductController } from "../controllers/business/ProductController";
import { ProfileController as BusinessProfileController } from "../controllers/business/ProfileController";
import { SharedController } from "../controllers/shared/SharedController";
import { handleEnsureAuthenticated } from "../middleware/common";

const authController = new AuthController();
const verifyAccountController = new VerifyAccountController();
const sharedController = new SharedController();
const businessController = new BusinessController();
const brandController = new BrandController();
const productController = new ProductController();
const businessProfileController = new BusinessProfileController();
const businessProductController = new BusinessProductController();

export default [
    {
        path: "register",
        method: "post",
        handler: authController.register
    },
    {
        path: "login",
        method: "post",
        handler: authController.login
    },
    {
        path: "verify/:token/user/:id",
        method: "get",
        handler: verifyAccountController.index
    },
    {
        path: "shared/countries",
        method: "get",
        handler: sharedController.getCountries
    },
    {
        path: "shared/countries/:country/cities",
        method: "get",
        handler: sharedController.getCities
    },
    {
        path: "shared/brands",
        method: "get",
        handler: sharedController.getBrands
    },
    {
        path: "shared/brand/:brand/products",
        method: "get",
        handler: sharedController.getProducts
    },
    {
        path: "shared/product/:product/presentations",
        method: "get",
        handler: sharedController.getPresentations
    },
    {
        path: "admin/business",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            businessController.index
        ]
    },
    {
        path: "admin/business",
        method: "post",
        handler: [
            handleEnsureAuthenticated,
            businessController.save
        ]
    },
    {
        path: "admin/business/:id/edit",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            businessController.edit
        ]
    },
    {
        path: "admin/business/:id",
        method: "put",
        handler: [
            handleEnsureAuthenticated,
            businessController.update
        ]
    },
    {
        path: "admin/business/:id/condition",
        method: "put",
        handler: [
            handleEnsureAuthenticated,
            businessController.updateCondition
        ]
    },
    {
        path: "admin/business/:id",
        method: "delete",
        handler: [
            handleEnsureAuthenticated,
            businessController.destroy
        ]
    },
    {
        path: "admin/brand",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            brandController.index
        ]
    },
    {
        path: "admin/brand",
        method: "post",
        handler: [
            handleEnsureAuthenticated,
            brandController.save
        ]
    },
    {
        path: "admin/brand/:id",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            brandController.edit
        ]
    },
    {
        path: "admin/brand/:id",
        method: "put",
        handler: [
            handleEnsureAuthenticated,
            brandController.update
        ]
    },
    {
        path: "admin/brand/:id",
        method: "delete",
        handler: [
            handleEnsureAuthenticated,
            brandController.destroy
        ]
    },
    {
        path: "admin/product",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            productController.index
        ]
    },
    {
        path: "admin/product",
        method: "post",
        handler: [
            handleEnsureAuthenticated,
            productController.save
        ]
    },
    {
        path: "admin/product/:id",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            productController.edit
        ]
    },
    {
        path: "admin/product/:id",
        method: "put",
        handler: [
            handleEnsureAuthenticated,
            productController.update
        ]
    },
    {
        path: "admin/product/:id",
        method: "delete",
        handler: [
            handleEnsureAuthenticated,
            productController.destroy
        ]
    },
    {
        path: "business/profile/:id",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            businessProfileController.edit
        ]
    },
    {
        path: "business/profile/:id",
        method: "put",
        handler: [
            handleEnsureAuthenticated,
            businessProfileController.update
        ]
    },
    {
        path: "business/product",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            businessProductController.index
        ]
    },
    {
        path: "business/product",
        method: "post",
        handler: [
            handleEnsureAuthenticated,
            businessProductController.save
        ]
    },
    {
        path: "business/product/:id",
        method: "get",
        handler: [
            handleEnsureAuthenticated,
            businessProductController.edit
        ]
    },
    {
        path: "business/product/:id",
        method: "put",
        handler: [
            handleEnsureAuthenticated,
            businessProductController.update
        ]
    },
    {
        path: "business/product/:id",
        method: "delete",
        handler: [
            handleEnsureAuthenticated,
            businessProductController.destroy
        ]
    }
];

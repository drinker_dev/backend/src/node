import express from "express";
import passport from "passport";
import Jwt from "passport-jwt";
import LocalStrategy from "passport-local";

import { Middleware } from "../helpers/middleware";
import { Routes } from "../helpers/routes";
import { app } from "../keys";
import middleware from "../middleware";
import errorHandlers from "../middleware/errorHandlers";
import passportHandlers from "../middleware/passportHandlers";
import routes from "../routes";

export class App {
    private app: express.Application;
    private middleware: Middleware;
    private errorHandlers: Middleware;
    private passportHandlers: Middleware;
    private routes: Routes;

    constructor() {
        this.app = express();
        this.middleware = new Middleware(middleware, this.app);
        this.routes = new Routes(routes, this.app);
        this.errorHandlers = new Middleware(errorHandlers, this.app);
        this.passportHandlers = new Middleware(passportHandlers, this.app);
        this.config();
    }

    public run() {
        this.app.listen(app.port, () => {
            console.log(`Server started at http://${app.host}:${app.port}`);
        });
    }

    private config(): void {
        process.on("uncaughtException", (e) => {
            console.error(e);
            // process.exit(1);
        });

        process.on("unhandledRejection", (e) => {
            console.error(e);
            // process.exit(1);
        });

        this.passportHandlers.apply();
        this.middleware.apply();
        this.routes.apply();
        this.errorHandlers.apply();
    }

}

import bcrypt from "bcrypt";
import { Router } from "express";
import passport from "passport";
import Jwt from "passport-jwt";
import LocalStrategy from "passport-local";

import { IUser, User } from "../models/User";

export const handleLocalStrategy = (router: Router) => {
    passport.use(new LocalStrategy.Strategy({
        usernameField: "username",
        passwordField: "password",
        session: false
    }, async (username, password, done) => {
        console.log("ejecutando *callback verify* de estrategia local");
        const user = await User
                      .findOne({
                        $and: [
                          {
                            username:
                            {
                              $eq: username.toLowerCase(),
                              $exists: true
                            }
                          },
                          {
                            enabled: true
                          }
                        ]
                      });
        if (user && bcrypt.compareSync(password, user.password)) {
          return done(null, user);
        }
        return done(null, false);
    }));
};

export const handleJwtStrategy = (router: Router) => {
    const opts = {
        jwtFromRequest: Jwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET,
        algorithms: [process.env.JWT_ALGORITHM]
    };

    passport.use(new Jwt.Strategy(opts, (jwtPayload, done) => {
        console.log("ejecutando *callback verify* de estategia jwt");
        User.findOne({ _id: jwtPayload._id })
            .populate("profile", "country city")
            .select("username enabled role type profile")
            .then((data) => {
                if (data === null) { // no existe el usuario
                    // podríamos registrar el usuario
                    return done(null, false);
                } else {
                    /*encontramos el usuario así que procedemos a devolverlo para
                    inyectarlo en req.user de la petición en curso*/
                    return done(null, data);
                }
            })
            .catch((err) => done(err, null)); // si hay un error lo devolvemos
    }));
};

export default [handleLocalStrategy, handleJwtStrategy];

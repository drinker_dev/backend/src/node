import parser from "body-parser";
import cors from "cors";
import { NextFunction, Request, Response, Router } from "express";
import passport from "passport";
import { HTTP401Error, HTTP403Error } from "../utils/httpErrors";

export const handleCors = (router: Router) =>
  router.use(cors({ credentials: true, origin: true }));

export const handleBodyRequestParsing = (router: Router) => {
  router.use(parser.urlencoded({ extended: false }));
  router.use(parser.json());
};

export const handleEnsureAuthenticated = (req: Request, resp: Response, next: NextFunction) => {
  passport.authenticate("jwt", { session: false }, (err, user, info) => {
    console.log("ejecutando *callback auth* de authenticate para estrategia jwt");
    // si hubo un error relacionado con la validez del token (error en su firma, caducado, etc)
    if (info) { return next(new HTTP401Error(info.message)); }

    // si hubo un error en la consulta a la base de datos
    console.error(`err: ${err}`);
    if (err) { return next(err); }

    // si el token está firmado correctamente pero no pertenece a un usuario existente
    if (!user) { return next(new HTTP403Error("You are not allowed to access")); }

    // inyectamos los datos de usuario en la request
    req.user = user;
    next();
  })(req, resp, next);
};

export const handlePassportInit = (router: Router) => {
  router.use(passport.initialize());
};

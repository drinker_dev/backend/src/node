import { handleBodyRequestParsing, handleCors, handlePassportInit } from "./common";

export default [handleCors, handleBodyRequestParsing, handlePassportInit];

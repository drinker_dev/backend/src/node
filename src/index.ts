import { Database } from "./database";
import { App } from "./server";
import ValidateEnv from "./utils/validateEnv";

ValidateEnv.validateEnv();

const server = new App();
const db = new Database();

db.connect();
server.run();

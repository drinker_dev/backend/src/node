import { Mailable } from "../helpers/mailable";
import { IBusiness } from "../models/Business";
import { VerifyAccount } from "../models/VerifyAccount";

export class UserRegister extends Mailable {
    public data: any;
    constructor(from: string, to: string | string[], data: IBusiness) {
        super(from, to);
        this.data = data;
        this.sending();
    }

    protected async generateUrl(): Promise<string> {
        const { token } = await VerifyAccount.findOne(
                        {
                            user: this.data.user
                        }
                        )
                        .select("-_id token");
        return `${process.env.APP_URL}/verify/${token}/user/${this.data.user}`;
    }

    protected async template(): Promise<string> {
        return `
            Hola,<br>
            Por favor, haga clic en el link para verificar su correo. <br>
            <a href="${await this.generateUrl()}">Clic aqui para verificar</a>
        `;
    }

    private async sending() {
        super.send(`Hola ${this.data.name}, por favor verifica su cuenta en Drinker`, await this.template());
    }
}

export const app = {
    host: process.env.HOST || "127.0.0.1",
    port: process.env.PORT || 3000
};

export const database = {
    URI: `${process.env.DB_CONNECTION}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT || 27017}/${process.env.DB_DATABASE}`
};

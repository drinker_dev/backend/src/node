import { CollectionReference, DocumentData, DocumentReference, Firestore, OrderByDirection } from "@google-cloud/firestore";

export class FirebaseService {
    private readonly db: Firestore;

    constructor(db: Firestore) {
        this.db = db;
    }

    public async create(collectionName: string, object: DocumentData, id: string): Promise<void> {
        try {
            await this.db.collection(collectionName).doc(`${id}`).set(object);
        } catch (err) {
            console.error(err);
        }
    }
    /**
     * update
     */
    public async update(collectionName: string, object: DocumentData, id: string): Promise<void> {
        try {
            await this.db.collection(collectionName).doc(`${id}`).update({
                price: object.price,
                updatedAt: object.updatedAt
            });
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * destroy
     */
    public async destroy(collectionName: string, id: string): Promise<void> {
        try {
            await this.db.collection(collectionName).doc(`${id}`).delete();
        } catch (err) {
            console.error(err);
        }
    }

    public async sortBy(collectionName: string, orderBy: string = "", flag: OrderByDirection = "asc"): Promise<void> {
        const colletionRanking = this.db
            .collection(collectionName)
            .orderBy(orderBy, flag);
        colletionRanking.get().then((querySnapshot) => {
            let place = 1;
            querySnapshot.forEach((doc) => {
                this.db
                .doc(`${collectionName}/${doc.id}`)
                .update({
                    place,
                    // tslint:disable-next-line: max-line-length
                    oldPlace: place !== doc.data().place ? (doc.data().hasOwnProperty("place") ? doc.data().place : place) : place
                });
                ++place;
            });
        });
    }
}

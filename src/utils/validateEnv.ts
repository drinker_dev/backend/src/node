import { cleanEnv, str } from "envalid";

export default class ValidateEnv {
    public static validateEnv() {
        cleanEnv(process.env, {
            DB_CONNECTION: str(),
            DB_HOST: str(),
            DB_DATABASE: str()
        });
    }
}

import { NextFunction, Request, Response } from "express";
import { Business } from "../../models/Business";

export class ProfileController {
    /**
     * edit
     */
    public async edit(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Business
                        .findOne({ user: req.params.id })
                        .populate("country", "name")
                        .populate("city", "name")
                        .populate("user", "username enabled")
                        .select("-createdAt -updatetAt");
        } catch (err) {
            next(err);
        }
        return resp.json({ data });
    }

    /**
     * update
     */
    public async update(req: Request, resp: Response, next: NextFunction) {
        try {
            await Business.updateOne({ _id: req.params.id },
                { $set:
                    {
                        name: req.body.name,
                        rif: req.body.rif,
                        country: req.body.country,
                        city: req.body.city,
                        address: req.body.address,
                        geoLocation: req.body.geoLocation,
                        updatedAt: Date.now()
                    }
                });
            const data = await Business.findOne({ _id: req.params.id })
                            .populate("country", "name")
                            .populate("city", "name")
                            .select("-createdAt -updatetAt");
            resp.json({ data, message: "Se actualizó el registro correctamente." });
        } catch (err) {
            next(err);
        }
    }
}

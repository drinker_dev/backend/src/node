import { NextFunction, Request, Response } from "express";
import moment from "moment";

import db from "../../database/firebase";
import { FeedHome } from "../../models/FeedHome";
import { FirebaseService } from "../../utils/FirebaseService";

export class ProductController {
    /**
     * Firebase firestore used by CRUD methods.
     */
    private static firestore: FirebaseService;
    /**
     * Check if exists the document in the collection
     * @param req Request
     * @return Promise<boolean>
     */
    private static async existsProduct(req: Request): Promise<boolean> {
        return await FeedHome.exists({
            business: req.user.get("profile._id"),
            brand: req.body.brand,
            product: req.body.product,
            presentation: req.body.presentation,
        });
    }

    constructor() {
        ProductController.firestore = new FirebaseService(db);
    }

    /**
     * index
     */
    public async index(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await FeedHome.find({ business: req.user.get("profile._id") })
                    .populate("brand", "-_id name")
                    .populate("product", "-_id name");
        } catch (err) {
            next(err);
        }

        return resp.json({ data });
    }

    /**
     * save
     */
    public async save(req: Request, resp: Response, next: NextFunction) {
        if (await ProductController.existsProduct(req)) {
            return resp.status(403).json({
                status: "error",
                message: "Este producto ya está registrado!"
            });
        }
        let data: any;
        try {
            const document = new FeedHome({
                business: req.user.get("profile._id"),
                brand: req.body.brand,
                product: req.body.product,
                presentation: req.body.presentation,
                price: req.body.price,
                country: req.user.get("profile.country"),
                city: req.user.get("profile.city"),
            });
            data = await document.save();
            data = await data.populate("business", "name")
                             .populate("brand", "name")
                             .populate("product", "name")
                             .populate("country", "name")
                             .populate("city", "name")
                             .execPopulate();
        } catch (err) {
            if (String(err).startsWith("ValidationError: FeedHome validation failed")) {
                resp.status(403).json({
                    status: "error",
                    message: err._message
                });
            }
            console.error(`Error occurred while adding new user, ${err}.`);
        }

        resp.json({ data , message: "Se creó el registro correctamente." });

        const feed = {
            _id: JSON.parse(JSON.stringify(data._id)),
            brand: JSON.parse(JSON.stringify(data.brand)),
            product: JSON.parse(JSON.stringify(data.product)),
            presentation: data.presentation,
            price: data.price,
            business: JSON.parse(JSON.stringify(data.business)),
            country: JSON.parse(JSON.stringify(data.country)),
            city: JSON.parse(JSON.stringify(data.city)),
            createdAt: data.createdAt,
            updatedAt: data.updatedAt
        };
        await ProductController.firestore.create(`feed_home`, feed, data._id);
        await ProductController.firestore.sortBy(`feed_home`, "price");
    }
    /**
     * edit
     */
    public async edit(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await FeedHome
                        .findOne({ _id: req.params.id })
                        .populate("product", "name")
                        .populate("brand", "name")
                        .select({ createdAt: 0, updatedAt: 0 });
        } catch (err) {
            console.error(err);
        }
        return resp.json({ data });
    }

    /**
     * update
     */
    public async update(req: Request, resp: Response, next: NextFunction) {
        let data;
        const { price } = req.body;
        try {
            await FeedHome.updateOne({ _id: req.params.id },
                { $set:
                    {
                        price
                    }
                });
            data = await FeedHome.findOne({ _id: req.params.id })
                            .populate("product", "name")
                            .populate("brand", "name")
                            .select("-createdAt");
            resp.json({ data, message: "Se actualizó el registro correctamente." });
        } catch (err) {
            next(err);
        }
        await ProductController.firestore.update(`feed_home`, data, data._id);
        await ProductController.firestore.sortBy(`feed_home`, "price");
    }

    /**
     * destroy delete register in document
     * @param req The request received by the API Rest
     * @param res The response sent by the API Rest
     * @param next The next function executed in the app's middleware
     */
    public async destroy(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await FeedHome.findOne({ _id: req.params.id });
            data.remove();
        } catch (err) {
            next(err);
        }
        resp.json({ message: "Se eliminó el registro de forma correcta." });
        await ProductController.firestore.destroy(`feed_home`, data._id);
        await ProductController.firestore.sortBy(`feed_home`, "price");
    }
}

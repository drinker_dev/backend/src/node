import { NextFunction, Request, Response } from "express";

import { Business } from "../../models/Business";
import { VerifyAccount } from "../../models/VerifyAccount";

export class VerifyAccountController {
    public async index(req: Request, resp: Response, next: NextFunction) {
        const user = await VerifyAccount.exists({
            user: req.params.id,
            token: req.params.token
        });

        if (user) {
            VerifyAccount.deleteOne(
                {
                    user: req.params.id,
                    token: req.params.token
                }
            ).exec();

            Business.updateOne(
                {
                    user: req.params.id
                },
                {
                    $set: {
                        status: 1
                    }
                }
            ).exec();

            return resp.status(200).json({
                verified: true,
                message: "La cuenta fue verificada, pronto nos pondremos en contacto con usted."
            });
        }

        return resp.status(403).json({
            status: "error",
            message: "Este enlace no pudo ser verficado, si cree que ésta información es incorrecta pongase en contacto con nosotros."
        });
    }
}

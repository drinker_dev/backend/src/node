import bcrypt from "bcrypt";
import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";
import { ObjectId } from "mongodb";
import passport from "passport";

import { UserRegister } from "../../mail/UserRegister";
import { Admin } from "../../models/Admin";
import { Business } from "../../models/Business";
import { User } from "../../models/User";
import { VerifyAccount } from "../../models/VerifyAccount";
import { HTTP404Error } from "../../utils/httpErrors";

export class AuthController {
    private static async generateTokenVerification(user: ObjectId): Promise<void> {
        try {
            const token = bcrypt.hashSync(
                Math.floor((Math.random() * 100) + 54).toString(),
                parseInt(process.env.BCRYPT_ROUNDS)
            ).replace(/\//g, "");
            await VerifyAccount.create({
                token,
                user
            });
        } catch (e) {
            console.error(`Error occurred while adding new token verification, ${e}.`);
        }
    }

    public async register(req: Request, resp: Response, next: NextFunction) {
        let user;
        let profile;
        try {
            const hash = bcrypt.hashSync(req.body.password, parseInt(process.env.BCRYPT_ROUNDS));

            if (req.body.type.toLowerCase() === "business") {
                profile = await Business.create({
                    name: req.body.name,
                    rif: req.body.rif,
                    country: req.body.country,
                    city: req.body.city,
                    address: req.body.address,
                    geoLocation: req.body.geoLocation,
                    status: 0
                });
            }

            user = await User.create({
                username: req.body.username,
                password: hash,
                type: req.body.type,
                profile: profile.id
            });

            await profile.updateOne({ $set: { user: user._id }});
            profile.user = user.id;

            await AuthController.generateTokenVerification(user.id);

            // tslint:disable-next-line: no-unused-expression
            new UserRegister("Soporte Drinker <noreply@drink-er.com>", user.username, profile);

            return resp.json({
                message: "La cuenta se creo correctamente, consulte su correo para confirmar la cuenta."
            });
        } catch (e) {
            if (String(e).startsWith("MongoError: E11000 duplicate key error")) {
                resp.status(403).json({
                    status: "error",
                    message: "A user with the given email already exists."
                });
            }
            console.error(`Error occurred while adding new user, ${e}.`);
        }
    }

    public login(req: Request, resp: Response, next: NextFunction) {
        passport.authenticate("local", { session: false }, async (error, user) => {
            console.log("ejecutando *callback auth* de authenticate para estrategia local");

            // si hubo un error en el callback verify relacionado con la consulta de datos de usuario
            if (error || !user) {
                next(new HTTP404Error("username or password not correct."));
            } else {
                console.log("*** comienza generacion token*****");
                let profile;
                if (user.type === "Admin") {
                    profile = await Admin.findOne({ user: user._id }).select("firstName lastName status");
                }
                if (user.type === "Business") {
                    profile = await Business.findOne({ user: user._id }).select("name status country");
                }

                const payload = {
                    _id: user._id,
                    exp: Date.now() + parseInt(process.env.JWT_LIFETIME),
                    username: user.username,
                    role: user.role,
                    type: user.type,
                    enabled: user.enabled,
                    profile
                };

                /* NOTA: Si estuviesemos usando sesiones, al usar un callback personalizado,
                es nuestra responsabilidad crear la sesión.
                Por lo que deberiamos llamar a req.logIn(user, (error)=>{}) aquí*/

                /*solo indicamos el payload ya que el header ya lo crea la lib jsonwebtoken internamente
                para el calculo de la firma y así obtener el token*/
                const token = jwt.sign(
                    JSON.stringify(payload),
                    process.env.JWT_SECRET,
                    { algorithm: process.env.JWT_ALGORITHM });
                resp.json({ token });
            }
        })(req, resp, next);
    }
}

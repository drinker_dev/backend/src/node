import { NextFunction, Request, Response } from "express";

import { Brand } from "../../models/Brand";
import { City } from "../../models/City";
import { Country } from "../../models/Country";
import { Product } from "../../models/Product";

export class SharedController {
    public async getCountries(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Country
                        .find()
                        .select({ name: 1 }) ;
        } catch (e) {
            next(e);
        }
        return resp.json({ data });
    }

    /**
     * getCities
     */
    public async getCities(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await City
                        .find({ country: req.params.country })
                        .select("name");
        } catch (e) {
            next(e);
        }
        return resp.json({ data });
    }

    /**
     * getBrands
     */
    public async getBrands(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Brand
                        .find()
                        .select({ name: 1 });
        } catch (e) {
            next(e);
        }
        return resp.json({ data });
    }

    /**
     * getProducts
     */
    public async getProducts(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Product
                        .find({ brand: req.params.brand, country: req.query.country })
                        .select({ name: 1 });
        } catch (e) {
            next(e);
        }
        return resp.json({ data });
    }

    /**
     * getPresentations
     */
    public async getPresentations(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Product
                        .findOne({ _id: req.params.product })
                        .select("-_id presentation ");
        } catch (e) {
            next(e);
        }
        return resp.json({ data: data.presentation });
    }
}

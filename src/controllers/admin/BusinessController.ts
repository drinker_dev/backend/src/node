import { NextFunction, Request, Response } from "express";
import { Business } from "../../models/Business";
import { User } from "../../models/User";

export class BusinessController {
    /**
     * index
     */
    public async index(req: Request, resp: Response, next: NextFunction) {
        let cursor;
        try {
            cursor = await Business
                        .find()
                        .populate("user")
                        .populate("country", "name")
                        .populate("city", "name")
                        .sort({ createdAt: "desc" });
        } catch (err) {
            next(err);
        }
        return resp.json(cursor);
    }

    /**
     * save
     */
    public async save(req: Request, resp: Response, next: NextFunction) {
        try {
            const profile = new Business({
                name: req.body.name,
                rif: req.body.rif,
                country: req.body.country,
                city: req.body.city,
                address: req.body.address,
                geoLocation: req.body.geoLocation,
            });
            const document = new User({
                username: req.body.username,
                type: req.body.type,
                profile
            });
            const data = await document.save();
            return resp.json({ data, message: "Se creó el registro correctamente." });
        } catch (err) {
            next(err);
        }
    }

    /**
     * edit
     */
    public async edit(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Business
                        .findOne({ _id: req.params.id })
                        .populate("country", "name")
                        .populate("city", "name")
                        .populate("user", "username enabled")
                        .select("-createdAt -updatetAt");
        } catch (err) {
            next(err);
        }
        return resp.json({ data });
    }

    /**
     * updateCondition
     */
    public async updateCondition(req: Request, resp: Response, next: NextFunction) {
        try {
            await Business.updateOne({ _id: req.params.id },
                    { $set: { status: req.body.status, updatedAt: Date.now() } });
            await User.updateOne({ profile: req.params.id },
                    { $set: { enabled: req.body.enabled, updatedAt: Date.now() } });
            const data = await Business.findOne({ _id: req.params.id })
                            .populate("user", "-_id enabled")
                            .select("status");
            resp.json({ data, message: "Se actualizó el estado de forma correcta." });
        } catch (err) {
            next(err);
        }
    }

    /**
     * update
     */
    public async update(req: Request, resp: Response, next: NextFunction) {
        try {
            await Business.updateOne({ _id: req.params.id },
                { $set:
                    {
                        name: req.body.name,
                        rif: req.body.rif,
                        country: req.body.country,
                        city: req.body.city,
                        address: req.body.address,
                        geoLocation: req.body.geoLocation,
                        updatedAt: Date.now()
                    }
                });
            const data = await Business.findOne({ _id: req.params.id })
            .populate("country", "name")
            .populate("city", "name")
            .select("-createdAt -updatetAt");
            resp.json({ data, message: "Se actualizó el registro correctamente." });
        } catch (err) {
            next(err);
        }

    }

    /**
     * destroy
     */
    public async destroy(req: Request, resp: Response, next: NextFunction) {
        try {
            await User.findByIdAndRemove(req.params.id);
            resp.json({ message: "Se eliminó el registro de forma correcta." });
        } catch (err) {
            next(err);
        }
    }
}

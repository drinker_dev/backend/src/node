import { NextFunction, Request, Response } from "express";
import { Brand } from "../../models/Brand";

export class BrandController {
    /**
     * index
     */
    public async index(req: Request, resp: Response, next: NextFunction) {
        let cursor;
        try {
            cursor = await Brand
                        .find({})
                        .select({ name: 1, enabled: 1 })
                        .sort({ createdAt: "desc" });
        } catch (e) {
            next(e);
        }
        return resp.json(cursor);
    }

    /**
     * save
     */
    public async save(req: Request, resp: Response, next: NextFunction) {
        try {
            const date = Date.now();
            const document = new Brand({
                name: req.body.name,
                enabled: true,
                createdAt: date,
                updatedAt: date
            });
            const data = await document.save();
            return resp.json({ data, message: "Se creó el registro correctamente." });
        } catch (err) {
            next(err);
        }
    }

    /**
     * edit
     */
    public async edit(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Brand
                        .findOne({ _id: req.params.id })
                        .select({ name: 1, enabled: 1 });
        } catch (err) {
            next(err);
        }
        return resp.json({ data });
    }

    /**
     * update
     */
    public async update(req: Request, resp: Response, next: NextFunction) {
        try {
            await Brand.updateOne({ _id: req.params.id },
                { $set:
                    {
                        name: req.body.name,
                        enabled: req.body.enabled,
                        updatedAt: Date.now()
                    }
                });
            const data = await Brand.findOne({ _id: req.params.id })
            .select({ name: 1, country: 1, enabled: 1 });
            resp.json({ data, message: "Se actualizó el registro correctamente." });
        } catch (err) {
            next(err);
        }

    }

    /**
     * destroy
     */
    public async destroy(req: Request, resp: Response, next: NextFunction) {
        try {
            await Brand.deleteOne({ _id: req.params.id });
        } catch (err) {
            next(err);
        }
        resp.json({ message: "Se eliminó el registro de forma correcta." });
    }
}

import { NextFunction, Request, Response } from "express";
import { Product } from "../../models/Product";

export class ProductController {
    /**
     * index
     */
    public async index(req: Request, resp: Response, next: NextFunction) {
        let cursor;
        try {
            cursor = await Product
                        .find({})
                        .populate("country")
                        .populate("brand")
                        .select({ createdAt: 0, updatedAt: 0 })
                        .sort({ createdAt: -1 });
        } catch (e) {
            next(e);
        }
        return resp.json(cursor);
    }

    /**
     * save
     */
    public async save(req: Request, resp: Response, next: NextFunction) {
        try {
            const document = new Product({
                brand: req.body.brand,
                country: req.body.country,
                name: req.body.name,
                presentation: req.body.presentation
            });
            let data = await document.save();
            data = await data.populate("brand").populate("country").execPopulate();
            return resp.json({ data, message: "Se creó el registro correctamente." });
        } catch (err) {
            next(err);
        }
    }

    /**
     * edit
     */
    public async edit(req: Request, resp: Response, next: NextFunction) {
        let data;
        try {
            data = await Product
                        .findOne({ _id: req.params.id })
                        .populate("country", "name")
                        .populate("brand", "name")
                        .select({ createdAt: 0, updatedAt: 0 });
        } catch (err) {
            next(err);
        }
        return resp.json({ data });
    }

    /**
     * update
     */
    public async update(req: Request, resp: Response, next: NextFunction) {
        try {
            await Product.updateOne({ _id: req.params.id },
                { $set:
                    {
                        name: req.body.name,
                        country: req.body.country,
                        brand: req.body.brand,
                        presentation: req.body.presentation,
                        updatedAt: Date.now()
                    }
                });
            const data = await Product.findOne({ _id: req.params.id })
                            .populate("country", "name")
                            .populate("brand", "name")
                            .select({ createdAt: 0, updatedAt: 0 });
            resp.json({ data, message: "Se actualizó el registro correctamente." });
        } catch (err) {
            next(err);
        }
    }

    /**
     * destroy
     */
    public async destroy(req: Request, resp: Response, next: NextFunction) {
        try {
            await Product.deleteOne({ _id: req.params.id });
        } catch (err) {
            next(err);
        }
        resp.json({ message: "Se eliminó el registro de forma correcta." });
    }
}

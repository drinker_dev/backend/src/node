"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const envalid_1 = require("envalid");
class ValidateEnv {
    static validateEnv() {
        envalid_1.cleanEnv(process.env, {
            DB_CONNECTION: envalid_1.str(),
            DB_HOST: envalid_1.str(),
            DB_DATABASE: envalid_1.str()
        });
    }
}
exports.default = ValidateEnv;
//# sourceMappingURL=validateEnv.js.map
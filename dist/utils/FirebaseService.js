"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class FirebaseService {
    constructor(db) {
        this.db = db;
    }
    create(collectionName, object, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.db.collection(collectionName).doc(`${id}`).set(object);
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    /**
     * update
     */
    update(collectionName, object, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.db.collection(collectionName).doc(`${id}`).update({
                    price: object.price,
                    updatedAt: object.updatedAt
                });
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    /**
     * destroy
     */
    destroy(collectionName, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.db.collection(collectionName).doc(`${id}`).delete();
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    sortBy(collectionName, orderBy = "", flag = "asc") {
        return __awaiter(this, void 0, void 0, function* () {
            const colletionRanking = this.db
                .collection(collectionName)
                .orderBy(orderBy, flag);
            colletionRanking.get().then((querySnapshot) => {
                let place = 1;
                querySnapshot.forEach((doc) => {
                    this.db
                        .doc(`${collectionName}/${doc.id}`)
                        .update({
                        place,
                        // tslint:disable-next-line: max-line-length
                        oldPlace: place !== doc.data().place ? (doc.data().hasOwnProperty("place") ? doc.data().place : place) : place
                    });
                    ++place;
                });
            });
        });
    }
}
exports.FirebaseService = FirebaseService;
//# sourceMappingURL=FirebaseService.js.map
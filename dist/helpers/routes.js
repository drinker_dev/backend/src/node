"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Routes {
    constructor(routes, router) {
        this.routes = routes;
        this.router = router;
    }
    apply() {
        for (const route of this.routes) {
            const { method, path, handler } = route;
            this.router[method](`/api/v1/${path}`, handler);
        }
    }
}
exports.Routes = Routes;
//# sourceMappingURL=routes.js.map
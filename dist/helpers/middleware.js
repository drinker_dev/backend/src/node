"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Middleware {
    constructor(middleware, router) {
        this.middleware = middleware;
        this.router = router;
    }
    apply() {
        for (const f of this.middleware) {
            f(this.router);
        }
    }
}
exports.Middleware = Middleware;
//# sourceMappingURL=middleware.js.map
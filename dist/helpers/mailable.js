"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mailgun_js_1 = __importDefault(require("mailgun-js"));
class Mailable {
    constructor(from, to) {
        this.conexion = mailgun_js_1.default({
            apiKey: process.env.MAIL_API_KEY,
            domain: process.env.MAIL_DOMAIN
        });
        this.from = from;
        this.to = to;
    }
    /**
     * send email
     * @param subject subject of mail
     * @param message body of mail
     * @param type indicate if the message is text or html
     */
    send(subject, message, type = "html") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.conexion.messages().send({
                    from: this.from,
                    to: this.convertTargetToString(this.to),
                    subject,
                    [type]: message
                });
            }
            catch (e) {
                console.log(`error sending emial, ${e}`);
            }
        });
    }
    /**
     * convert the value from array type to string type, if It is an array
     * @param value this can be string or array type
     * @return string
     */
    convertTargetToString(value) {
        if (typeof value === "object") {
            return value.toString();
        }
        return value;
    }
}
exports.Mailable = Mailable;
//# sourceMappingURL=mailable.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const passport_1 = __importDefault(require("passport"));
const passport_jwt_1 = __importDefault(require("passport-jwt"));
const passport_local_1 = __importDefault(require("passport-local"));
const User_1 = require("../models/User");
exports.handleLocalStrategy = (router) => {
    passport_1.default.use(new passport_local_1.default.Strategy({
        usernameField: "username",
        passwordField: "password",
        session: false
    }, (username, password, done) => __awaiter(void 0, void 0, void 0, function* () {
        console.log("ejecutando *callback verify* de estrategia local");
        const user = yield User_1.User
            .findOne({
            $and: [
                {
                    username: {
                        $eq: username.toLowerCase(),
                        $exists: true
                    }
                },
                {
                    enabled: true
                }
            ]
        });
        if (user && bcrypt_1.default.compareSync(password, user.password)) {
            return done(null, user);
        }
        return done(null, false);
    })));
};
exports.handleJwtStrategy = (router) => {
    const opts = {
        jwtFromRequest: passport_jwt_1.default.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET,
        algorithms: [process.env.JWT_ALGORITHM]
    };
    passport_1.default.use(new passport_jwt_1.default.Strategy(opts, (jwtPayload, done) => {
        console.log("ejecutando *callback verify* de estategia jwt");
        User_1.User.findOne({ _id: jwtPayload._id })
            .populate("profile", "country city")
            .select("username enabled role type profile")
            .then((data) => {
            if (data === null) { // no existe el usuario
                // podríamos registrar el usuario
                return done(null, false);
            }
            else {
                /*encontramos el usuario así que procedemos a devolverlo para
                inyectarlo en req.user de la petición en curso*/
                return done(null, data);
            }
        })
            .catch((err) => done(err, null)); // si hay un error lo devolvemos
    }));
};
exports.default = [exports.handleLocalStrategy, exports.handleJwtStrategy];
//# sourceMappingURL=passportHandlers.js.map
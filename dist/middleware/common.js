"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const passport_1 = __importDefault(require("passport"));
const httpErrors_1 = require("../utils/httpErrors");
exports.handleCors = (router) => router.use(cors_1.default({ credentials: true, origin: true }));
exports.handleBodyRequestParsing = (router) => {
    router.use(body_parser_1.default.urlencoded({ extended: false }));
    router.use(body_parser_1.default.json());
};
exports.handleEnsureAuthenticated = (req, resp, next) => {
    passport_1.default.authenticate("jwt", { session: false }, (err, user, info) => {
        console.log("ejecutando *callback auth* de authenticate para estrategia jwt");
        // si hubo un error relacionado con la validez del token (error en su firma, caducado, etc)
        console.log(`info: ${info}`);
        if (info) {
            return next(new httpErrors_1.HTTP401Error(info.message));
        }
        // si hubo un error en la consulta a la base de datos
        console.log(`err: ${err}`);
        if (err) {
            return next(err);
        }
        // si el token está firmado correctamente pero no pertenece a un usuario existente
        if (!user) {
            return next(new httpErrors_1.HTTP403Error("You are not allowed to access"));
        }
        // inyectamos los datos de usuario en la request
        req.user = user;
        next();
    })(req, resp, next);
};
exports.handlePassportInit = (router) => {
    router.use(passport_1.default.initialize());
};
//# sourceMappingURL=common.js.map
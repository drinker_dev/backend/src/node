"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BrandController_1 = require("../controllers/admin/BrandController");
const BusinessController_1 = require("../controllers/admin/BusinessController");
const ProductController_1 = require("../controllers/admin/ProductController");
const authController_1 = require("../controllers/auth/authController");
const VerifyAccountController_1 = require("../controllers/auth/VerifyAccountController");
const ProductController_2 = require("../controllers/business/ProductController");
const ProfileController_1 = require("../controllers/business/ProfileController");
const SharedController_1 = require("../controllers/shared/SharedController");
const common_1 = require("../middleware/common");
const authController = new authController_1.AuthController();
const verifyAccountController = new VerifyAccountController_1.VerifyAccountController();
const sharedController = new SharedController_1.SharedController();
const businessController = new BusinessController_1.BusinessController();
const brandController = new BrandController_1.BrandController();
const productController = new ProductController_1.ProductController();
const businessProfileController = new ProfileController_1.ProfileController();
const businessProductController = new ProductController_2.ProductController();
exports.default = [
    {
        path: "register",
        method: "post",
        handler: authController.register
    },
    {
        path: "login",
        method: "post",
        handler: authController.login
    },
    {
        path: "verify/:token/user/:id",
        method: "get",
        handler: verifyAccountController.index
    },
    {
        path: "shared/countries",
        method: "get",
        handler: sharedController.getCountries
    },
    {
        path: "shared/countries/:country/cities",
        method: "get",
        handler: sharedController.getCities
    },
    {
        path: "shared/brands",
        method: "get",
        handler: sharedController.getBrands
    },
    {
        path: "shared/brand/:brand/products",
        method: "get",
        handler: sharedController.getProducts
    },
    {
        path: "shared/product/:product/presentations",
        method: "get",
        handler: sharedController.getPresentations
    },
    {
        path: "admin/business",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessController.index
        ]
    },
    {
        path: "admin/business",
        method: "post",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessController.save
        ]
    },
    {
        path: "admin/business/:id/edit",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessController.edit
        ]
    },
    {
        path: "admin/business/:id",
        method: "put",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessController.update
        ]
    },
    {
        path: "admin/business/:id/condition",
        method: "put",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessController.updateCondition
        ]
    },
    {
        path: "admin/business/:id",
        method: "delete",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessController.destroy
        ]
    },
    {
        path: "admin/brand",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            brandController.index
        ]
    },
    {
        path: "admin/brand",
        method: "post",
        handler: [
            common_1.handleEnsureAuthenticated,
            brandController.save
        ]
    },
    {
        path: "admin/brand/:id",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            brandController.edit
        ]
    },
    {
        path: "admin/brand/:id",
        method: "put",
        handler: [
            common_1.handleEnsureAuthenticated,
            brandController.update
        ]
    },
    {
        path: "admin/brand/:id",
        method: "delete",
        handler: [
            common_1.handleEnsureAuthenticated,
            brandController.destroy
        ]
    },
    {
        path: "admin/product",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            productController.index
        ]
    },
    {
        path: "admin/product",
        method: "post",
        handler: [
            common_1.handleEnsureAuthenticated,
            productController.save
        ]
    },
    {
        path: "admin/product/:id",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            productController.edit
        ]
    },
    {
        path: "admin/product/:id",
        method: "put",
        handler: [
            common_1.handleEnsureAuthenticated,
            productController.update
        ]
    },
    {
        path: "admin/product/:id",
        method: "delete",
        handler: [
            common_1.handleEnsureAuthenticated,
            productController.destroy
        ]
    },
    {
        path: "business/profile/:id",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessProfileController.edit
        ]
    },
    {
        path: "business/profile/:id",
        method: "put",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessProfileController.update
        ]
    },
    {
        path: "business/product",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessProductController.index
        ]
    },
    {
        path: "business/product",
        method: "post",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessProductController.save
        ]
    },
    {
        path: "business/product/:id",
        method: "get",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessProductController.edit
        ]
    },
    {
        path: "business/product/:id",
        method: "put",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessProductController.update
        ]
    },
    {
        path: "business/product/:id",
        method: "delete",
        handler: [
            common_1.handleEnsureAuthenticated,
            businessProductController.destroy
        ]
    }
];
//# sourceMappingURL=routes.js.map
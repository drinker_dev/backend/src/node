"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Product_1 = require("../../models/Product");
class ProductController {
    /**
     * index
     */
    index(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let cursor;
            try {
                cursor = yield Product_1.Product
                    .find({})
                    .populate("country")
                    .populate("brand")
                    .select({ createdAt: 0, updatedAt: 0 })
                    .sort({ createdAt: -1 });
            }
            catch (e) {
                next(e);
            }
            return resp.json(cursor);
        });
    }
    /**
     * save
     */
    save(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const document = new Product_1.Product({
                    brand: req.body.brand,
                    country: req.body.country,
                    name: req.body.name,
                    presentation: req.body.presentation
                });
                let data = yield document.save();
                data = yield data.populate("brand").populate("country").execPopulate();
                return resp.json({ data, message: "Se creó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * edit
     */
    edit(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Product_1.Product
                    .findOne({ _id: req.params.id })
                    .populate("country", "name")
                    .populate("brand", "name")
                    .select({ createdAt: 0, updatedAt: 0 });
            }
            catch (err) {
                next(err);
            }
            return resp.json({ data });
        });
    }
    /**
     * update
     */
    update(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Product_1.Product.updateOne({ _id: req.params.id }, { $set: {
                        name: req.body.name,
                        country: req.body.country,
                        brand: req.body.brand,
                        presentation: req.body.presentation,
                        updatedAt: Date.now()
                    }
                });
                const data = yield Product_1.Product.findOne({ _id: req.params.id })
                    .populate("country", "name")
                    .populate("brand", "name")
                    .select({ createdAt: 0, updatedAt: 0 });
                resp.json({ data, message: "Se actualizó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * destroy
     */
    destroy(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Product_1.Product.deleteOne({ _id: req.params.id });
            }
            catch (err) {
                next(err);
            }
            resp.json({ message: "Se eliminó el registro de forma correcta." });
        });
    }
}
exports.ProductController = ProductController;
//# sourceMappingURL=ProductController.js.map
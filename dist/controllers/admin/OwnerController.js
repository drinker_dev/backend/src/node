"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("../../models/user");
class OwnerController {
    /**
     * index
     */
    index(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let cursor;
            try {
                cursor = yield user_1.User
                    .find({ type: "Owner" })
                    .select({ profile: 1, enabled: 1 });
            }
            catch (err) {
                next(err);
            }
            return resp.json(cursor);
        });
    }
    /**
     * save
     */
    save(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const document = new user_1.User({
                    username: req.body.username,
                    type: req.body.type,
                    profile: {
                        name: req.body.name,
                        rif: req.body.rif,
                        country: req.body.country,
                        city: req.body.city,
                        address: req.body.address,
                        geoLocation: req.body.geoLocation,
                        status: 0
                    }
                });
                const owner = yield document.save();
                return resp.json({ data: yield owner.save(), message: "Se creó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * edit
     */
    edit(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let cursor;
            try {
                cursor = yield user_1.User
                    .findOne({ $and: [{ _id: req.params.id }, { type: "Owner" }] })
                    .select({ username: 1, profile: 1, enabled: 1 });
            }
            catch (err) {
                next(err);
            }
            return resp.json(cursor);
        });
    }
    /**
     * updateCondition
     */
    updateCondition(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let cursor;
            try {
                cursor = yield user_1.User.findOne({ _id: req.params.id })
                    .select({ "profile.status": 1, "enabled": 1 });
                cursor.profile.status = req.body.status;
                cursor.enabled = req.body.enabled;
                cursor.save();
            }
            catch (err) {
                next(err);
            }
            resp.json({ data: cursor, message: "Se actualizó el estado de forma correcta." });
        });
    }
    /**
     * update
     */
    update(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let cursor;
            try {
                cursor = yield user_1.User.findOne({ _id: req.params.id })
                    .select({ profile: 1 });
                cursor.profile.name = req.body.name;
                cursor.profile.rif = req.body.rif;
                cursor.profile.country = req.body.country;
                cursor.profile.city = req.body.city;
                cursor.profile.address = req.body.address;
                cursor.profile.geoLocation = req.body.geoLocation;
                cursor.updatedAt = new Date();
                cursor.save();
            }
            catch (err) {
                next(err);
            }
            resp.json({ data: cursor, message: "Se actualizó el registro correctamente." });
        });
    }
    /**
     * destroy
     */
    destroy(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield user_1.User.findByIdAndRemove(req.params.id);
                resp.json({ message: "Se eliminó el registro de forma correcta." });
            }
            catch (err) {
                next(err);
            }
        });
    }
}
exports.OwnerController = OwnerController;
//# sourceMappingURL=OwnerController.js.map
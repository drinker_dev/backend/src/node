"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Brand_1 = require("../../models/Brand");
class BrandController {
    /**
     * index
     */
    index(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("BrandController.index");
            let cursor;
            try {
                cursor = yield Brand_1.Brand
                    .find({})
                    .select({ name: 1, enabled: 1 })
                    .sort({ createdAt: "desc" });
            }
            catch (e) {
                next(e);
            }
            return resp.json(cursor);
        });
    }
    /**
     * save
     */
    save(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const date = Date.now();
                const document = new Brand_1.Brand({
                    name: req.body.name,
                    enabled: true,
                    createdAt: date,
                    updatedAt: date
                });
                const data = yield document.save();
                return resp.json({ data, message: "Se creó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * edit
     */
    edit(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Brand_1.Brand
                    .findOne({ _id: req.params.id })
                    .select({ name: 1, enabled: 1 });
            }
            catch (err) {
                next(err);
            }
            return resp.json({ data });
        });
    }
    /**
     * update
     */
    update(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Brand_1.Brand.updateOne({ _id: req.params.id }, { $set: {
                        name: req.body.name,
                        enabled: req.body.enabled,
                        updatedAt: Date.now()
                    }
                });
                const data = yield Brand_1.Brand.findOne({ _id: req.params.id })
                    .select({ name: 1, country: 1, enabled: 1 });
                resp.json({ data, message: "Se actualizó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * destroy
     */
    destroy(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Brand_1.Brand.deleteOne({ _id: req.params.id });
            }
            catch (err) {
                next(err);
            }
            resp.json({ message: "Se eliminó el registro de forma correcta." });
        });
    }
}
exports.BrandController = BrandController;
//# sourceMappingURL=BrandController.js.map
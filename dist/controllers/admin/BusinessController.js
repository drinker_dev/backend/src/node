"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Business_1 = require("../../models/Business");
const User_1 = require("../../models/User");
class BusinessController {
    /**
     * index
     */
    index(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let cursor;
            try {
                cursor = yield Business_1.Business
                    .find()
                    .populate("user")
                    .populate("country", "name")
                    .populate("city", "name")
                    .sort({ createdAt: "desc" });
            }
            catch (err) {
                next(err);
            }
            return resp.json(cursor);
        });
    }
    /**
     * save
     */
    save(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const profile = new Business_1.Business({
                    name: req.body.name,
                    rif: req.body.rif,
                    country: req.body.country,
                    city: req.body.city,
                    address: req.body.address,
                    geoLocation: req.body.geoLocation,
                });
                const document = new User_1.User({
                    username: req.body.username,
                    type: req.body.type,
                    profile
                });
                const data = yield document.save();
                return resp.json({ data, message: "Se creó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * edit
     */
    edit(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Business_1.Business
                    .findOne({ _id: req.params.id })
                    .populate("country", "name")
                    .populate("city", "name")
                    .populate("user", "username enabled")
                    .select("-createdAt -updatetAt");
            }
            catch (err) {
                next(err);
            }
            return resp.json({ data });
        });
    }
    /**
     * updateCondition
     */
    updateCondition(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Business_1.Business.updateOne({ _id: req.params.id }, { $set: { status: req.body.status, updatedAt: Date.now() } });
                yield User_1.User.updateOne({ profile: req.params.id }, { $set: { enabled: req.body.enabled, updatedAt: Date.now() } });
                const data = yield Business_1.Business.findOne({ _id: req.params.id })
                    .populate("user", "-_id enabled")
                    .select("status");
                resp.json({ data, message: "Se actualizó el estado de forma correcta." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * update
     */
    update(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Business_1.Business.updateOne({ _id: req.params.id }, { $set: {
                        name: req.body.name,
                        rif: req.body.rif,
                        country: req.body.country,
                        city: req.body.city,
                        address: req.body.address,
                        geoLocation: req.body.geoLocation,
                        updatedAt: Date.now()
                    }
                });
                const data = yield Business_1.Business.findOne({ _id: req.params.id })
                    .populate("country", "name")
                    .populate("city", "name")
                    .select("-createdAt -updatetAt");
                resp.json({ data, message: "Se actualizó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
    /**
     * destroy
     */
    destroy(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield User_1.User.findByIdAndRemove(req.params.id);
                resp.json({ message: "Se eliminó el registro de forma correcta." });
            }
            catch (err) {
                next(err);
            }
        });
    }
}
exports.BusinessController = BusinessController;
//# sourceMappingURL=BusinessController.js.map
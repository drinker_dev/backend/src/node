"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const system_1 = require("../../models/system");
class SystemController {
    getCountries(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("CountryController.getCountries");
            let cursor;
            try {
                cursor = yield system_1.System
                    .find({})
                    .select({ name: 1 });
            }
            catch (e) {
                next(e);
            }
            return resp.json(cursor);
        });
    }
    /**
     * getCities
     */
    getCities(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("CountryController.getCities", req.params.country);
            let cursor;
            try {
                cursor = yield system_1.System
                    .findById(req.params.country)
                    .select({ _id: 0, cities: 1 });
            }
            catch (e) {
                next(e);
            }
            return resp.json(cursor);
        });
    }
}
exports.SystemController = SystemController;
//# sourceMappingURL=systemController.js.map
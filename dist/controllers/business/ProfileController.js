"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Business_1 = require("../../models/Business");
class ProfileController {
    /**
     * edit
     */
    edit(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Business_1.Business
                    .findOne({ user: req.params.id })
                    .populate("country", "name")
                    .populate("city", "name")
                    .populate("user", "username enabled")
                    .select("-createdAt -updatetAt");
            }
            catch (err) {
                next(err);
            }
            return resp.json({ data });
        });
    }
    /**
     * update
     */
    update(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Business_1.Business.updateOne({ _id: req.params.id }, { $set: {
                        name: req.body.name,
                        rif: req.body.rif,
                        country: req.body.country,
                        city: req.body.city,
                        address: req.body.address,
                        geoLocation: req.body.geoLocation,
                        updatedAt: Date.now()
                    }
                });
                const data = yield Business_1.Business.findOne({ _id: req.params.id })
                    .populate("country", "name")
                    .populate("city", "name")
                    .select("-createdAt -updatetAt");
                resp.json({ data, message: "Se actualizó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
        });
    }
}
exports.ProfileController = ProfileController;
//# sourceMappingURL=ProfileController.js.map
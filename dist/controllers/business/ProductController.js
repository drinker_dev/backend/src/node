"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_1 = __importDefault(require("../../database/firebase"));
const FeedHome_1 = require("../../models/FeedHome");
const FirebaseService_1 = require("../../utils/FirebaseService");
class ProductController {
    constructor() {
        ProductController.firestore = new FirebaseService_1.FirebaseService(firebase_1.default);
    }
    /**
     * Check if exists the document in the collection
     * @param req Request
     * @return Promise<boolean>
     */
    static existsProduct(req) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield FeedHome_1.FeedHome.exists({
                business: req.user.get("profile._id"),
                brand: req.body.brand,
                product: req.body.product,
                presentation: req.body.presentation,
            });
        });
    }
    /**
     * index
     */
    index(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield FeedHome_1.FeedHome.find({ business: req.user.get("profile._id") })
                    .populate("brand", "-_id name")
                    .populate("product", "-_id name");
            }
            catch (err) {
                next(err);
            }
            return resp.json({ data });
        });
    }
    /**
     * save
     */
    save(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            if (yield ProductController.existsProduct(req)) {
                return resp.status(403).json({
                    status: "error",
                    message: "Este producto ya está registrado!"
                });
            }
            let data;
            try {
                const document = new FeedHome_1.FeedHome({
                    business: req.user.get("profile._id"),
                    brand: req.body.brand,
                    product: req.body.product,
                    presentation: req.body.presentation,
                    price: req.body.price,
                    country: req.user.get("profile.country"),
                    city: req.user.get("profile.city"),
                });
                data = yield document.save();
                data = yield data.populate("business", "name")
                    .populate("brand", "name")
                    .populate("product", "name")
                    .populate("country", "name")
                    .populate("city", "name")
                    .execPopulate();
            }
            catch (err) {
                if (String(err).startsWith("ValidationError: FeedHome validation failed")) {
                    resp.status(403).json({
                        status: "error",
                        message: err._message
                    });
                }
                console.error(`Error occurred while adding new user, ${err}.`);
            }
            resp.json({ data, message: "Se creó el registro correctamente." });
            const feed = {
                _id: JSON.parse(JSON.stringify(data._id)),
                brand: JSON.parse(JSON.stringify(data.brand)),
                product: JSON.parse(JSON.stringify(data.product)),
                presentation: data.presentation,
                price: data.price,
                business: JSON.parse(JSON.stringify(data.business)),
                country: JSON.parse(JSON.stringify(data.country)),
                city: JSON.parse(JSON.stringify(data.city)),
                createdAt: data.createdAt,
                updatedAt: data.updatedAt
            };
            yield ProductController.firestore.create(`feed_home`, feed, data._id);
            yield ProductController.firestore.sortBy(`feed_home`, "price");
        });
    }
    /**
     * edit
     */
    edit(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield FeedHome_1.FeedHome
                    .findOne({ _id: req.params.id })
                    .populate("product", "name")
                    .populate("brand", "name")
                    .select({ createdAt: 0, updatedAt: 0 });
            }
            catch (err) {
                console.error(err);
            }
            return resp.json({ data });
        });
    }
    /**
     * update
     */
    update(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            const { price } = req.body;
            try {
                yield FeedHome_1.FeedHome.updateOne({ _id: req.params.id }, { $set: {
                        price
                    }
                });
                data = yield FeedHome_1.FeedHome.findOne({ _id: req.params.id })
                    .populate("product", "name")
                    .populate("brand", "name")
                    .select("-createdAt");
                resp.json({ data, message: "Se actualizó el registro correctamente." });
            }
            catch (err) {
                next(err);
            }
            yield ProductController.firestore.update(`feed_home`, data, data._id);
            yield ProductController.firestore.sortBy(`feed_home`, "price");
        });
    }
    /**
     * destroy delete register in document
     * @param req The request received by the API Rest
     * @param res The response sent by the API Rest
     * @param next The next function executed in the app's middleware
     */
    destroy(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield FeedHome_1.FeedHome.findOne({ _id: req.params.id });
                data.remove();
            }
            catch (err) {
                next(err);
            }
            resp.json({ message: "Se eliminó el registro de forma correcta." });
            yield ProductController.firestore.destroy(`feed_home`, data._id);
            yield ProductController.firestore.sortBy(`feed_home`, "price");
        });
    }
}
exports.ProductController = ProductController;
//# sourceMappingURL=ProductController.js.map
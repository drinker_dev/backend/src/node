"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const passport_1 = __importDefault(require("passport"));
const UserRegister_1 = require("../../mail/UserRegister");
const Admin_1 = require("../../models/Admin");
const Business_1 = require("../../models/Business");
const User_1 = require("../../models/User");
const VerifyAccount_1 = require("../../models/VerifyAccount");
const httpErrors_1 = require("../../utils/httpErrors");
class AuthController {
    static generateTokenVerification(user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const token = bcrypt_1.default.hashSync(Math.floor((Math.random() * 100) + 54).toString(), parseInt(process.env.BCRYPT_ROUNDS)).replace(/\//g, "");
                yield VerifyAccount_1.VerifyAccount.create({
                    token,
                    user
                });
            }
            catch (e) {
                console.error(`Error occurred while adding new token verification, ${e}.`);
            }
        });
    }
    register(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let user;
            let profile;
            try {
                const hash = bcrypt_1.default.hashSync(req.body.password, parseInt(process.env.BCRYPT_ROUNDS));
                if (req.body.type.toLowerCase() === "business") {
                    profile = yield Business_1.Business.create({
                        name: req.body.name,
                        rif: req.body.rif,
                        country: req.body.country,
                        city: req.body.city,
                        address: req.body.address,
                        geoLocation: req.body.geoLocation,
                        status: 0
                    });
                }
                user = yield User_1.User.create({
                    username: req.body.username,
                    password: hash,
                    type: req.body.type,
                    profile: profile.id
                });
                yield profile.updateOne({ $set: { user: user._id } });
                profile.user = user.id;
                yield AuthController.generateTokenVerification(user.id);
                // tslint:disable-next-line: no-unused-expression
                new UserRegister_1.UserRegister("Soporte Drinker <noreply@drink-er.com>", user.username, profile);
                return resp.json({
                    message: "La cuenta se creo correctamente, consulte su correo para confirmar la cuenta."
                });
            }
            catch (e) {
                if (String(e).startsWith("MongoError: E11000 duplicate key error")) {
                    resp.status(403).json({
                        status: "error",
                        message: "A user with the given email already exists."
                    });
                }
                console.error(`Error occurred while adding new user, ${e}.`);
            }
        });
    }
    login(req, resp, next) {
        console.log("login");
        passport_1.default.authenticate("local", { session: false }, (error, user) => __awaiter(this, void 0, void 0, function* () {
            console.log("ejecutando *callback auth* de authenticate para estrategia local");
            // si hubo un error en el callback verify relacionado con la consulta de datos de usuario
            if (error || !user) {
                next(new httpErrors_1.HTTP404Error("username or password not correct."));
            }
            else {
                console.log("*** comienza generacion token*****");
                let profile;
                if (user.type === "Admin") {
                    profile = yield Admin_1.Admin.findOne({ user: user._id }).select("firstName lastName status");
                }
                if (user.type === "Business") {
                    profile = yield Business_1.Business.findOne({ user: user._id }).select("name status country");
                }
                console.log(yield User_1.User.find({ username: user.username }).populate("profile"));
                const payload = {
                    _id: user._id,
                    exp: Date.now() + parseInt(process.env.JWT_LIFETIME),
                    username: user.username,
                    role: user.role,
                    type: user.type,
                    enabled: user.enabled,
                    profile
                };
                /* NOTA: Si estuviesemos usando sesiones, al usar un callback personalizado,
                es nuestra responsabilidad crear la sesión.
                Por lo que deberiamos llamar a req.logIn(user, (error)=>{}) aquí*/
                /*solo indicamos el payload ya que el header ya lo crea la lib jsonwebtoken internamente
                para el calculo de la firma y así obtener el token*/
                const token = jsonwebtoken_1.default.sign(JSON.stringify(payload), process.env.JWT_SECRET, { algorithm: process.env.JWT_ALGORITHM });
                resp.json({ token });
            }
        }))(req, resp, next);
    }
}
exports.AuthController = AuthController;
//# sourceMappingURL=authController.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Business_1 = require("../../models/Business");
const VerifyAccount_1 = require("../../models/VerifyAccount");
class VerifyAccountController {
    index(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("req.params", req.params);
            const user = yield VerifyAccount_1.VerifyAccount.exists({
                user: req.params.id,
                token: req.params.token
            });
            if (user) {
                VerifyAccount_1.VerifyAccount.deleteOne({
                    user: req.params.id,
                    token: req.params.token
                }).exec();
                Business_1.Business.updateOne({
                    user: req.params.id
                }, {
                    $set: {
                        status: 1
                    }
                }).exec();
                return resp.status(200).json({
                    verified: true,
                    message: "La cuenta fue verificada, pronto nos pondremos en contacto con usted."
                });
            }
            return resp.status(403).json({
                status: "error",
                message: "Este enlace no pudo ser verficado, si cree que ésta información es incorrecta pongase en contacto con nosotros."
            });
        });
    }
}
exports.VerifyAccountController = VerifyAccountController;
//# sourceMappingURL=VerifyAccountController.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Brand_1 = require("../../models/Brand");
const City_1 = require("../../models/City");
const Country_1 = require("../../models/Country");
const Product_1 = require("../../models/Product");
class SharedController {
    getCountries(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Country_1.Country
                    .find()
                    .select({ name: 1 });
            }
            catch (e) {
                next(e);
            }
            return resp.json({ data });
        });
    }
    /**
     * getCities
     */
    getCities(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield City_1.City
                    .find({ country: req.params.country })
                    .select("name");
            }
            catch (e) {
                next(e);
            }
            return resp.json({ data });
        });
    }
    /**
     * getBrands
     */
    getBrands(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Brand_1.Brand
                    .find()
                    .select({ name: 1 });
            }
            catch (e) {
                next(e);
            }
            return resp.json({ data });
        });
    }
    /**
     * getProducts
     */
    getProducts(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Product_1.Product
                    .find({ brand: req.params.brand, country: req.query.country })
                    .select({ name: 1 });
            }
            catch (e) {
                next(e);
            }
            return resp.json({ data });
        });
    }
    /**
     * getPresentations
     */
    getPresentations(req, resp, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            try {
                data = yield Product_1.Product
                    .findOne({ _id: req.params.product })
                    .select("-_id presentation ");
            }
            catch (e) {
                next(e);
            }
            return resp.json({ data: data.presentation });
        });
    }
}
exports.SharedController = SharedController;
//# sourceMappingURL=SharedController.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mailable_1 = require("../helpers/mailable");
const VerifyAccount_1 = require("../models/VerifyAccount");
class UserRegister extends mailable_1.Mailable {
    constructor(from, to, data) {
        super(from, to);
        this.data = data;
        this.sending();
    }
    generateUrl() {
        return __awaiter(this, void 0, void 0, function* () {
            const { token } = yield VerifyAccount_1.VerifyAccount.findOne({
                user: this.data.user
            })
                .select("-_id token");
            console.log(token);
            return `${process.env.APP_URL}/verify/${token}/user/${this.data.user}`;
        });
    }
    template() {
        return __awaiter(this, void 0, void 0, function* () {
            return `
            Hola,<br>
            Por favor, haga clic en el link para verificar su correo. <br>
            <a href="${yield this.generateUrl()}">Clic aqui para verificar</a>
        `;
        });
    }
    sending() {
        const _super = Object.create(null, {
            send: { get: () => super.send }
        });
        return __awaiter(this, void 0, void 0, function* () {
            _super.send.call(this, `Hola ${this.data.name}, por favor verifica su cuenta en Drinker`, yield this.template());
        });
    }
}
exports.UserRegister = UserRegister;
//# sourceMappingURL=UserRegister.js.map
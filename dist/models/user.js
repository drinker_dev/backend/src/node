"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const Owner_1 = require("./Owner");
const UserSchema = new mongoose_1.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: String,
    enabled: {
        type: Boolean,
        default: false
    },
    type: {
        type: String,
        required: true
    },
    profile: Owner_1.OwnerSchema,
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
    role: [String]
});
exports.User = mongoose_1.model("User", UserSchema, "users");
//# sourceMappingURL=user.js.map
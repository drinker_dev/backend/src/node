"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const GeoJSON_1 = require("./GeoJSON");
exports.BusinessSchema = new mongoose_1.Schema({
    user: {
        type: mongoose_1.Types.ObjectId,
        ref: "User",
    },
    name: {
        type: String,
        required: true
    },
    rif: {
        type: String,
        required: true,
        unique: true
    },
    country: {
        type: mongoose_1.Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: mongoose_1.Types.ObjectId,
        ref: "City",
        required: true
    },
    address: {
        type: String,
        required: true
    },
    geoLocation: GeoJSON_1.SchemaPoint,
    status: {
        type: Number,
        enum: [0, 1, 2],
        default: 0
    }
}, {
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});
exports.Business = mongoose_1.model("Business", exports.BusinessSchema, "business");
//# sourceMappingURL=Business.js.map
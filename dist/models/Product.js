"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ProductSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    brand: {
        type: mongoose_1.Types.ObjectId,
        ref: "Brand",
        required: true
    },
    presentation: {
        type: [String]
    },
    country: {
        type: mongoose_1.Types.ObjectId,
        ref: "Country",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});
exports.Product = mongoose_1.model("Product", exports.ProductSchema, "products");
//# sourceMappingURL=Product.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ProductDetail_1 = require("./ProductDetail");
exports.FeedBusinessSchema = new mongoose_1.Schema({
    business: {
        type: mongoose_1.Types.ObjectId,
        ref: "Business",
        required: true
    },
    average: {
        type: Number
    },
    country: {
        type: mongoose_1.Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: mongoose_1.Types.ObjectId,
        ref: "City",
        required: true
    },
    products: [ProductDetail_1.ProductDetailSchema]
}, {
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});
exports.FeedBusiness = mongoose_1.model("FeedBusiness", exports.FeedBusinessSchema, "feed_business");
//# sourceMappingURL=FeedBusiness.js.map
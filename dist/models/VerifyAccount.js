"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const VerifyAccountSchema = new mongoose_1.Schema({
    user: {
        type: mongoose_1.Types.ObjectId,
        ref: "User",
        required: true,
        unique: true
    },
    token: {
        type: String,
        required: true
    }
}, {
    timestamps: {
        createdAt: "createdAt"
    }
});
exports.VerifyAccount = mongoose_1.model("VerifyAccount", VerifyAccountSchema, "verify_accounts");
//# sourceMappingURL=VerifyAccount.js.map
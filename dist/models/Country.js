"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const GeoJSON_1 = require("./GeoJSON");
exports.CountrySchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    iso: String,
    geoLocation: GeoJSON_1.SchemaPoint,
    cities: {
        type: mongoose_1.Types.ObjectId,
        ref: "City",
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});
exports.Country = mongoose_1.model("Country", exports.CountrySchema, "countries");
//# sourceMappingURL=Country.js.map
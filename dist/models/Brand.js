"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.BrandSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    enabled: {
        type: Boolean
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});
exports.Brand = mongoose_1.model("Brand", exports.BrandSchema, "brands");
//# sourceMappingURL=Brand.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.SchemaPoint = new mongoose_1.Schema({
    type: {
        $type: String,
        enum: ["Point"],
        required: true
    },
    coordinates: {
        $type: [Number],
        required: true
    }
}, {
    _id: false,
    typeKey: "$type",
});
//# sourceMappingURL=GeoJSON.js.map
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const FeedBusiness_1 = require("./FeedBusiness");
const FeedProduct_1 = require("./FeedProduct");
exports.FeedHomeSchema = new mongoose_1.Schema({
    business: {
        type: mongoose_1.Types.ObjectId,
        ref: "Business",
        required: true
    },
    brand: {
        type: mongoose_1.Types.ObjectId,
        ref: "Brand",
        required: true
    },
    product: {
        type: mongoose_1.Types.ObjectId,
        ref: "Product",
        required: true
    },
    presentation: String,
    price: {
        type: Number,
        required: true
    },
    country: {
        type: mongoose_1.Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: mongoose_1.Types.ObjectId,
        ref: "City",
        required: true
    }
}, {
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});
exports.FeedHomeSchema.post("save", (doc) => __awaiter(void 0, void 0, void 0, function* () {
    let product;
    let business;
    if (!(yield FeedProduct_1.FeedProduct.exists({ "product": doc.get("product"), "business._id": doc.get("business") }))) {
        product = {
            brand: doc.get("brand"),
            product: doc.get("product"),
            presentation: doc.get("presentation"),
            country: doc.get("country"),
            city: doc.get("city"),
            createdAt: doc.get("createdAt"),
            updatedAt: doc.get("updatedAt")
        };
        business = {
            business: {
                _id: doc.get("business"),
                price: doc.get("price"),
                updatedAt: doc.get("updatedAt")
            }
        };
        yield FeedProduct_1.FeedProduct.updateOne({
            product: doc.get("product")
        }, {
            $set: product,
            $addToSet: business
        }, {
            upsert: true
        });
    }
    if (!(yield FeedBusiness_1.FeedBusiness.exists({ "business": doc.get("business"), "products._id": doc.get("product") }))) {
        business = {
            business: doc.get("business"),
            country: doc.get("country"),
            city: doc.get("city"),
            createdAt: doc.get("createdAt"),
            updatedAt: doc.get("updatedAt")
        };
        product = {
            products: {
                _id: doc.get("product"),
                brand: doc.get("brand"),
                presentation: doc.get("presentation"),
                price: doc.get("price"),
                updatedAt: doc.get("updatedAt")
            }
        };
        yield FeedBusiness_1.FeedBusiness.updateOne({
            business: doc.get("business")
        }, {
            $set: business,
            $addToSet: product
        }, {
            upsert: true
        });
    }
}));
exports.FeedHomeSchema.post("updateOne", function () {
    return __awaiter(this, void 0, void 0, function* () {
        const docToUpdate = yield this.model.findOne(this.getQuery());
        yield FeedProduct_1.FeedProduct.updateOne({
            "product": docToUpdate.product,
            "business._id": docToUpdate.business
        }, {
            $set: {
                "business.$.price": docToUpdate.price,
                "business.$.updatedAt": docToUpdate.updatedAt
            }
        });
        yield FeedBusiness_1.FeedBusiness.updateOne({
            "business": docToUpdate.business,
            "products._id": docToUpdate.product
        }, {
            $set: {
                "products.$.price": docToUpdate.price,
                "products.$.updatedAt": docToUpdate.updatedAt
            }
        });
    });
});
exports.FeedHomeSchema.post("remove", (doc) => __awaiter(void 0, void 0, void 0, function* () {
    yield FeedProduct_1.FeedProduct.updateOne({
        product: doc.get("product")
    }, {
        $pull: {
            business: {
                _id: doc.get("business")
            }
        }
    });
    yield FeedBusiness_1.FeedBusiness.updateOne({
        business: doc.get("business")
    }, {
        $pull: {
            products: {
                _id: doc.get("product")
            }
        }
    });
}));
exports.FeedHome = mongoose_1.model("FeedHome", exports.FeedHomeSchema, "feed_home");
//# sourceMappingURL=FeedHome.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const BusinessDetail_1 = require("./BusinessDetail");
exports.FeedProductSchema = new mongoose_1.Schema({
    brand: {
        type: mongoose_1.Types.ObjectId,
        ref: "Brand",
        required: true
    },
    product: {
        type: mongoose_1.Types.ObjectId,
        ref: "Product",
        required: true
    },
    presentation: String,
    average: {
        type: Number
    },
    business: [BusinessDetail_1.BusinessDetailSchema],
    country: {
        type: mongoose_1.Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: mongoose_1.Types.ObjectId,
        ref: "City",
        required: true
    }
}, {
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});
exports.FeedProduct = mongoose_1.model("FeedProduct", exports.FeedProductSchema, "feed_products");
//# sourceMappingURL=FeedProduct.js.map
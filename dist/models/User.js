"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const UserSchema = new mongoose_1.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: String,
    enabled: {
        type: Boolean,
        default: false
    },
    type: {
        type: String,
        required: true,
        enum: ["Admin", "Business", "People"]
    },
    role: [String],
    profile: {
        type: mongoose_1.Types.ObjectId,
        required: true,
        // Instead of a hardcoded model name in `ref`, `refPath` means Mongoose
        // will look at the `onModel` property to find the right model.
        refPath: "type"
    }
}, {
    timestamps: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
});
exports.User = mongoose_1.model("User", UserSchema, "users");
//# sourceMappingURL=User.js.map
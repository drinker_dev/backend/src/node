"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const CountrySchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    iso: {
        type: String,
        required: true
    },
    geo_location: Array,
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
});
exports.Country = mongoose_1.model("Country", CountrySchema, "countries");
//# sourceMappingURL=country.js.map
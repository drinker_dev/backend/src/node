"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.BusinessDetailSchema = new mongoose_1.Schema({
    _id: {
        type: mongoose_1.Types.ObjectId,
        ref: "Business",
        required: true
    },
    price: {
        type: Number,
        required: true
    }
}, {
    timestamps: {
        updatedAt: "updatedAt"
    },
    _id: false
});
exports.BusinessDetail = mongoose_1.model("BusinessDetail", exports.BusinessDetailSchema);
//# sourceMappingURL=BusinessDetail.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const GeoJSON_1 = require("./GeoJSON");
exports.OwnerSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    rif: {
        type: String,
        required: true,
        unique: true
    },
    country: {
        type: Object,
        required: true
    },
    city: {
        type: Object,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    geoLocation: GeoJSON_1.SchemaPoint,
    status: {
        type: Number,
        enum: [0, 1, 2],
        required: true
    },
    createdAt: {
        type: Date
    },
    updatedAt: {
        type: Date
    }
});
exports.Owner = mongoose_1.model("Owner", exports.OwnerSchema, "owners");
//# sourceMappingURL=Owner.js.map
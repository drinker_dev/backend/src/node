"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ProductDetailSchema = new mongoose_1.Schema({
    _id: {
        type: mongoose_1.Types.ObjectId,
        ref: "Product",
        required: true
    },
    brand: {
        type: mongoose_1.Types.ObjectId,
        ref: "Brand",
        required: true
    },
    presentation: String,
    price: {
        type: Number,
        required: true
    }
}, {
    timestamps: {
        updatedAt: "updatedAt"
    },
    _id: false
});
exports.ProductDetail = mongoose_1.model("ProductDetail", exports.ProductDetailSchema);
//# sourceMappingURL=ProductDetail.js.map
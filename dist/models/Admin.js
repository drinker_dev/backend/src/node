"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.AdminSchema = new mongoose_1.Schema({
    user: {
        type: mongoose_1.Types.ObjectId,
        ref: "User",
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    identityDocument: {
        type: String,
        required: true
    },
    country: {
        type: mongoose_1.Types.ObjectId,
        ref: "Country",
        required: true
    },
    city: {
        type: mongoose_1.Types.ObjectId,
        ref: "City",
        required: true
    },
    address: {
        type: String,
        required: true
    },
    status: {
        type: Number,
        enum: [0, 1, 2],
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});
exports.Admin = mongoose_1.model("Admin", exports.AdminSchema, "administrations");
//# sourceMappingURL=Admin.js.map
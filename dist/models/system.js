"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const SystemSchema = new mongoose_1.Schema({
    menu: {
        type: Array
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});
exports.System = mongoose_1.model("System", SystemSchema, "system");
//# sourceMappingURL=system.js.map
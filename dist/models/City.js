"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const GeoJSON_1 = require("./GeoJSON");
exports.CitySchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true
    },
    state: String,
    capital: String,
    geoLocation: GeoJSON_1.SchemaPoint,
    country: {
        type: mongoose_1.Types.ObjectId,
        ref: "Country",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});
exports.City = mongoose_1.model("City", exports.CitySchema, "cities");
//# sourceMappingURL=City.js.map
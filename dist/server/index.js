"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const middleware_1 = require("../helpers/middleware");
const routes_1 = require("../helpers/routes");
const keys_1 = require("../keys");
const middleware_2 = __importDefault(require("../middleware"));
const errorHandlers_1 = __importDefault(require("../middleware/errorHandlers"));
const passportHandlers_1 = __importDefault(require("../middleware/passportHandlers"));
const routes_2 = __importDefault(require("../routes"));
class App {
    constructor() {
        this.app = express_1.default();
        this.middleware = new middleware_1.Middleware(middleware_2.default, this.app);
        this.routes = new routes_1.Routes(routes_2.default, this.app);
        this.errorHandlers = new middleware_1.Middleware(errorHandlers_1.default, this.app);
        this.passportHandlers = new middleware_1.Middleware(passportHandlers_1.default, this.app);
        this.config();
    }
    run() {
        this.app.listen(keys_1.app.port, () => {
            console.log(`Server started at http://${keys_1.app.host}:${keys_1.app.port}`);
        });
    }
    config() {
        process.on("uncaughtException", (e) => {
            console.log(e);
            process.exit(1);
        });
        process.on("unhandledRejection", (e) => {
            console.log(e);
            process.exit(1);
        });
        this.passportHandlers.apply();
        this.middleware.apply();
        this.routes.apply();
        this.errorHandlers.apply();
    }
}
exports.App = App;
//# sourceMappingURL=index.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("./database");
const server_1 = require("./server");
const validateEnv_1 = __importDefault(require("./utils/validateEnv"));
validateEnv_1.default.validateEnv();
const server = new server_1.App();
const db = new database_1.Database();
db.connect();
server.run();
//# sourceMappingURL=index.js.map